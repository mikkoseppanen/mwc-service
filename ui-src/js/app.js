(function(ng, mz) {
  'use strict';

  // create app module
  var app = ng.module('mwcApp', ['ngAnimate', 'ngTouch', 'ngFileUpload']);

  // Fix up the template tags so that they don't collide with golang  tags.
  app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
  }]);

  // Add externals as constants for use with DI
  app.constant('Modernizr', mz);
})(
  angular,
  Modernizr
);
