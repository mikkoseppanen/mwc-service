(function(ng) {
  'use strict';
  ng.module('mwcApp').controller('IndexCtrl', IndexCtrl);

  function IndexCtrl($scope, $rootScope, $timeout, Upload) {
    'ngInject';

    // Get a reference to self, that we can use in callbacks
    var ctrl = this;

    // Loaded flag
    this.loaded = false;

    // Report data
    this.report = null;

    // Default sorting key
    this.reportSortBy = 'employeeName';

    // Any errors that might've happened during upload
    this.reportError = null;

    // upload on file select or drop
    this.upload = function (file) {
        // reset error.
        ctrl.reportError = null;

        if ($scope.upload.csv.$valid && file) {
          Upload.upload({
              url: '/api/csv',
              data: {csv: file}
          }).then(function (resp) {
              ctrl.report = resp.data;

          }, function (resp) {
              ctrl.reportError = resp.status + ' ' + resp.statusText;
              ctrl.report = null;
          }, ng.nop);

        } else {
          ctrl.report = null;
          ctrl.reportError = "Try selecting a CSV file (supported mime types are text/csv, text/plain)";
        }
    };

    // sort changes sorting column for report table, inversing order if same
    // key is selected twice.
    this.sort = function(col) {
      var re = new RegExp('^\-?' + col + '$');

      if (re.test(this.reportSortBy)) {
        this.reportSortBy = (this.reportSortBy.indexOf('-') === 0 ? '' : '-') + col;

      } else {
        this.reportSortBy = col;
      }
    };

    // sortIs returns if given key is the active sorting column
    this.sortIs = function(col) {
      var re = new RegExp('^\-?' + col + '$');
      var classes = "";
      if (re.test(this.reportSortBy) === true) {
        classes = "selected";

        if (this.reportSortBy.indexOf('-') === 0) {
          classes += " desc";
        }
      }

      return classes;
    };

    // dirty init method
    this.init = function() {
      // Use a fake timeout to actually make "boot" visible
      $timeout(function() {
        ctrl.loaded = true;
      }, 250);
    };
  }
})(angular);
