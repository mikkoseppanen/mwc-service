# Monthly Wage Calculator

Simple frontend for the mwc-service.

## Requirements
 * npm
  * `apt-get install npm`
 * grunt and grunt CLI are required.
  * `npm install -g grunt grunt-cli`
 * imagemagick for favicon processing
  * `apt-get install imagemagick`

## Installing dependencies
 * `npm install`

## Building
 * `grunt dev`
  * Dev build with minimal asset compiling / minification
 * `grunt dist`
  * Distribution build with minification, asset bundling and favicon generation.
 * `grunt genfavicons`
  * Generate favicons.

`dev` and `dist` targets will build into `dist/` directory and upon completion
transfer the resources into `../ui`.
