module.exports = function(grunt) {
  grunt.initConfig({
    clean: {
      dist: {
        src: ['dist/', 'tmp']
      }
    },
    jshint: {
      dist: ['gruntfile.js', 'js/**/*.js', 'test/**/*.js']
    },
    modernizr: {
      dist: {
        'crawl': false,
        'customTests': [],
        'dest': 'tmp/modernizr/modernizr.js',
        'tests': [
          'flexbox',
          'flexboxlegacy',
          'flexboxtweener',
          'flexwrap',
          'fontface',
          'mediaqueries',
          'csstransitions'
        ],
        'options': [
          'setClasses',
          'html5shiv'
        ],
        'uglify': true
      }
    },
    concat: {
      options: {
        process: function(src, filepath) {
          return src.replace(/\n\/\/# sourceMappingURL=.*\.map\n/g, '\n\n') ;
        }
      },
      dist: {
        src: [
          'js/app.js',
          'js/**/service.js',
          'js/**/directive.js',
          'js/**/controller.js'
        ],
        dest: 'tmp/out/js/app.min.js'
      },
      libs: {
        src: [
          'node_modules/es5-shim/es5-shim.min.js',
          'tmp/modernizr/modernizr.js',
          'node_modules/angular/angular.min.js',
          'node_modules/angular-animate/angular-animate.min.js',
          'node_modules/angular-touch/angular-touch.min.js',
          'node_modules/ng-file-upload/dist/ng-file-upload-shim.min.js',
          'node_modules/ng-file-upload/dist/ng-file-upload.min.js'

        ],
        dest: 'tmp/out/js/vendor.js'
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      dist: {
        files: {
          'tmp/out/js/app.min.js': 'tmp/out/js/app.min.js'
        }
      }
    },
    uglify: {
      options: {
        screwIE8: true,
        preserveComments: false
      },
      dist: {
        files: {
          'tmp/out/js/app.min.js': ['tmp/out/js/app.min.js']
        }
      }
    },
    less: {
      dev: {
        options: {
          compress: false,
          paths: ['assets/less'],
          plugins: [
            new (require('less-plugin-autoprefix'))({
              browsers: ['> 5%', 'last 3 versions', 'firefox > 3.6', 'ie > 9']
            })
          ]
        },
        files: {
          'tmp/out/css/index.min.css': 'assets/less/index.less',
          'tmp/out/css/inline.min.css': [
            'node_modules/normalize.css/normalize.css',
            'assets/less/inline.less'
          ]
        }
      },
      dist: {
        options: {
          compress: true,
          paths: ['assets/less'],
          plugins: [
            new (require('less-plugin-autoprefix'))({
              browsers: ['> 5%', 'last 3 versions', 'firefox > 3.6', 'ie > 9']
            }),
            new (require('less-plugin-clean-css'))({keepSpecialComments: 1})
          ],
        },
        files: {
          'tmp/out/css/index.min.css': 'assets/less/index.less',
          'tmp/out/css/inline.min.css': [
            'node_modules/normalize.css/normalize.css',
            'assets/less/inline.less'
          ]
        }
      }
    },
    favicons: {
      options: {
        appleTouchBackgroundColor: '#004155',
        tileBlackWhite: false,
        tileColor: '#004155',
        HTMLPrefix: '/images/favicons/'
      },
      icons: {
        src: 'assets/favicon/favicon.png',
        dest: 'dist/images/favicons/'
      }
    },
    copy: {
      js: {
        expand: true,
        cwd: 'tmp/out',
        src: 'js/**/*.js',
        dest: 'dist/'
      },
      css: {
        expand: true,
        cwd: 'tmp/out',
        src: 'css/*.css',
        dest: 'dist/'
      },
      installTemplates: {
        src: 'templates/*.html',
        dest: '../ui/'
      },
      installApp: {
        expand: true,
        cwd: 'dist',
        src: '**/*',
        dest: '../ui/'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-favicons');
  grunt.loadNpmTasks('grunt-inline');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-modernizr');
  grunt.registerTask('dist', [
    'clean',
    'jshint',
    'modernizr',
    'concat',
    'ngAnnotate',
    'uglify',
    'less:dist',
    'copy:css',
    'copy:js',
    'favicons',
    'copy:installTemplates',
    'copy:installApp',
  ]);
  grunt.registerTask('dev', [
    'clean',
    'jshint',
    'modernizr',
    'concat',
    'ngAnnotate',
    'less:dev',
    'copy:css',
    'copy:js',
    'copy:install'
  ]);
  grunt.registerTask('genfavicons', [
    'favicons'
  ]);
};
