package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

// setupRouter maps paths to callbacks and returns a router.
func setupRouter() *mux.Router {
	r := mux.NewRouter()

	// Handler for parsing API
	r.HandleFunc("/api/csv", handleCSV).Methods("POST")

	// Handler for index queries
	r.HandleFunc("/", handleRoot).Methods("GET")

	// Add a file server for static content
	s := http.StripPrefix("/static", http.FileServer(http.Dir(uiPath)))
	r.PathPrefix("/static/{css,js,images}").Handler(s)

	return r
}
