package main

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
)

type templateData struct {
	Hostname string
}

// Handler for people asking for root path, we give them the index
func handleRoot(w http.ResponseWriter, r *http.Request) {
	tpath := filepath.Join(uiPath, "templates/base.html")
	csspath := filepath.Join(uiPath, "css/inline.min.css")
	t, err := template.New("base.html").ParseFiles(tpath, csspath)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	p := templateData{
		Hostname: hostName,
	}

	err = t.Execute(w, &p)
	if err != nil {
		log.Println(err.Error())
	}
}
