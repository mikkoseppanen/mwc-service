package main

import "bitbucket.org/mikkoseppanen/nut"

// CompactReport describes the report document generated from the input shift data.
type CompactReport struct {
	Date string       `json:"date"`
	Rows []CompactRow `json:"rows"`
}

// CompactRow is a single entry in a CompactReport. Contains values for a
// single employee
type CompactRow struct {
	ID            string  `json:"employeeID"`
	Name          string  `json:"employeeName"`
	Wage          float64 `json:"wage"`
	HoursTotal    float64 `json:"hoursTotal"`
	HoursRegular  float64 `json:"hoursRegular"`
	HoursEvening  float64 `json:"hoursEvening"`
	HoursOvertime float64 `json:"hoursOvertime"`
}

// NewCompactReport returns a new report initialized from a nut.Report
func NewCompactReport(report *nut.Report) *CompactReport {
	res := &CompactReport{Date: report.DateString()}

	// Calculate per employee compensations and create a compact row of each,
	// We'll have a nice precalculated document that can be encoded out eg. as
	// JSON.
	for _, emp := range report.Employees {
		rr := CompactRow{
			ID:   emp.ID,
			Name: emp.Name,
		}

		empReport := emp.Compensation()
		rr.HoursEvening = empReport.HoursEvening.Hours()
		rr.HoursOvertime = empReport.HoursOvertime.Hours()
		rr.HoursRegular = empReport.HoursRegular.Hours()
		rr.HoursTotal = empReport.HoursTotal.Hours()
		rr.Wage = nut.ToFixed(float64(empReport.Wage)/100.0, 2)

		res.Rows = append(res.Rows, rr)
	}

	return res
}
