package main

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"bitbucket.org/mikkoseppanen/nut"
)

// handler for /csv path. Attempts to parse submitted CSV data and output
// results as a JSON.
func handleCSV(w http.ResponseWriter, r *http.Request) {
	ct := r.Header.Get("Content-Type")
	var reader io.Reader
	defer r.Body.Close()

	// Change read source based on input data Content-Type
	switch {
	// File upload
	case strings.HasPrefix(strings.ToLower(ct), "multipart/form-data;"):
		f, h, err := r.FormFile("csv")
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		defer f.Close()

		// Check that content type is CSV or plain text.
		if strings.EqualFold(h.Header.Get("Content-Type"), "text/csv") == false &&
			strings.EqualFold(h.Header.Get("Content-Type"), "text/plain") == false {
			http.Error(w, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)
			return
		}

		reader = f

	// Data is the body as is
	case strings.HasPrefix(strings.ToLower(ct), "application/x-www-form-urlencoded"):
		csvdata := r.PostFormValue("csv")
		reader = strings.NewReader(csvdata)

	default:
		http.Error(w, http.StatusText(http.StatusUnsupportedMediaType), http.StatusUnsupportedMediaType)
		return
	}

	// Get a CSV parser
	parser := nut.NewCSVParser(reader)

	// Parse input
	doc, err := nut.GenerateReport(parser)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Transform to a precalculated document
	compactDoc := NewCompactReport(doc)

	// Output result as a JSON doc.
	enc := json.NewEncoder(w)
	if err := enc.Encode(&compactDoc); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
