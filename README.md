# Monthly Wage Calculator service

A very simple web service for displaying sortable reports from CSV shift data.

## Structure

Base directory contains the service backend source files. These files build into
the service binary, that provides a very simple API for transforming CSV data
into JSON reports, using the [nut](https://bitbucket.org/mikkoseppanen/nut) library.

`ui/` contains precompiled UI resources, eg. templates, application JS and CSS.

`ui-src/` contains source codes for the UI resources with grunt and NPM configs
for installing dependencies and building the resources.

## Depedencies

 * Uses [nut](https://bitbucket.org/mikkoseppanen/nut) for parsing the input data.
 * Uses [gorilla/mux](github.com/gorilla/mux) for routing requests.

## Acquiring

`go get -u bitbucket.org/mikkoseppanen/mwc-service`

## Building

`go build bitbucket.org/mikkoseppanen/mwc-service`

## Installing

`go install bitbucket.org/mikkoseppanen/mwc-service`

## Usage

`mwc-service --help`

## Notes

Please refer to [Golang getting started](https://golang.org/doc/install) for information on how to setup Golang for your OS.
