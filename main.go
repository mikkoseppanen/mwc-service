package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

var hostAddr string
var hostName string
var uiPath string

// Parse command line arguments in init, less clutter in main.
func init() {
	listen := flag.String("listen", ":8100", "Listen address:port")
	static := flag.String("static", "", "Path to frontend static resources. If empty, lookup is attempted from $GOPATH")
	hostname := flag.String("hostname", "", "Hostname, if none is given, listen address is used.")

	flag.Parse()

	if *hostname == "" {
		*hostname = *listen
	}

	if *static == "" {
		gopath := os.Getenv("GOPATH")
		if gopath == "" {
			log.Fatalln("$GOPATH not set and no explicit static path given")
		}

		*static = filepath.Join(gopath, "src/bitbucket.org/mikkoseppanen/mwc-service/ui")
	}

	absPath, err := filepath.Abs(*static)
	if err != nil {
		log.Fatalln(err.Error())
	}

	hostAddr = *listen
	hostName = *hostname
	uiPath = absPath
}

func main() {

	// Init router
	router := setupRouter()

	log.Printf("Starting HTTP service, listening in '%s', name '%s', static content at '%s'\n",
		hostAddr,
		hostName,
		uiPath,
	)

	log.Fatalf("Failed to start HTTP service: %s", http.ListenAndServe(hostAddr, router))
}
